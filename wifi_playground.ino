// #define led_cervena 14
#include "ESP8266WiFi.h"
void setup() {
/*  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(led_cervena, OUTPUT);*/

  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
}

void loop() {
/*  // put your main code here, to run repeatedly:
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);

  digitalWrite(led_cervena, HIGH);
  delay(1000);
  digitalWrite(led_cervena, LOW);
  delay(1000);*/

  Serial.println("Zahajuji skenovani");

  int n = WiFi.scanNetworks();

  if(n == 0) {
    Serial.println("Zadne viditelne WiFi v okoli.");
  }
  else {
    Serial.print(n);
    Serial.println(" WiFi siti v okoli.");
    Serial.println("Seznam dostupnych siti:");

    for(int i = 0; i < n; ++i) {
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.print((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
      delay(10);
    }
  }
  Serial.println("");
  delay(5000);
}
